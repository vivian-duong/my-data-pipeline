#!/bin/bash
# keep container from running
# Docker containers exit when its main process finishes
# found @ 
# https://github.com/sequenceiq/hadoop-docker/blob/master/bootstrap.sh
while true; do sleep 1000; done