import mypipeline
from google.cloud import bigquery

dataset_id = 'traffic_cam'
mypipeline.create_dataset(dataset_id)

sql_RLL = """
                create table traffic_cam.red_light_camera_locations(INTERSECTION STRING, 
                													FIRST_APPROACH STRING, 
                													SECOND_APPROACH STRING, 
                													THIRD_APPROACH STRING, 
                													GO_LIVE_DATE DATE, 
                													LATITUDE FLOAT64, 
                													LONGITUDE FLOAT64, 
                													LOCATION STRING, 
                													Historical_Wards_2003_2015 INT64, 
                													Zip_Codes INT64, 
                													Community_Areas INT64, 
                													Census_Tracts INT64, 
                													Wards INT64)
            """

sql_RLV = """
			create table traffic_cam.red_light_camera_violations(INTERSECTION STRING, 
																CAMERA_ID STRING, 
																ADDRESS STRING, 
																VIOLATION_DATE DATE, 
																VIOLATIONS NUMERIC, 
																X_COORDINATE NUMERIC, 
																Y_COORDINATE NUMERIC, 
																LATITUDE FLOAT64, 
																LONGITUDE FLOAT64, 
																LOCATION STRING, 
																Historical_Wards_2003_2015 INT64, 
																Zip_Codes INT64, 
																Community_Areas FLOAT64, 
																Census_Tracts FLOAT64, 
																Wards FLOAT64)
"""

sql_SCL = """
			create table traffic_cam.speed_camera_locations(ADDRESS STRING, 
															FIRST_APPROACH STRING, 
															SECOND_APPROACH STRING, 
															GO_LIVE_DATE DATE, 
															LATITUDE FLOAT64, 
															LONGITUDE FLOAT64, 
															LOCATION STRING, 
															Historical_Wards_2003_2015 INT64, 
															Zip_Codes INT64, 
															Community_Areas FLOAT64, 
															Census_Tracts FLOAT64, 
															Wards FLOAT64)
"""

sql_SCV = """
			create table traffic_cam.speed_camera_violations(ADDRESS STRING, 
															 CAMERA_ID STRING, 
															 VIOLATION_DATE DATE, 
															 VIOLATIONS NUMERIC, 
															 X_COORDINATE NUMERIC, 
															 Y_COORDINATE NUMERIC, 
															 LATITUDE FLOAT64, 
															 LONGITUDE FLOAT64, 
															 LOCATION STRING, 
															 Historical_Wards_2003_2015 INT64, 
															 Zip_Codes INT64, 
															 Community_Areas FLOAT64, 
															 Census_Tracts FLOAT64, 
															 Wards FLOAT64)										
"""

sql_files_ingested = """create table traffic_cam.files_ingested(name string,
                                                                arrival_date date,
                                                                origin string)"""
def create_my_tables():
    sqls_set_schemas = [sql_RLL, sql_RLV, sql_SCL, sql_SCV, sql_files_ingested]
    tuple(map(bigquery.Client().query, sqls_set_schemas))

if __name__ == '__main__':
	create_my_tables()