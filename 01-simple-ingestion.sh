#!/bin/bash

if [ ! -d csv-files ]
then
	echo unzipping csv-files.zip
	unzip csv-files.zip
else
	echo csv-files.zip was already unzipped
fi

# $* means all files in directory
for f in csv-files/*200*.csv
do
    python 01-simple-ingestion.py $f 
done

for f in csv-files/*201[0-7]*.csv
do
    python 01-simple-ingestion.py $f
done