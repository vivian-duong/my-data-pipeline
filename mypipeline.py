from google.cloud import bigquery
from google.cloud import storage
from google.api_core import exceptions
import os
import zipfile

dataset_id = 'traffic_cam'

def create_dataset(dataset_id):
    client = bigquery.Client()
    dataset = bigquery.Dataset(f'{client.project}.{dataset_id}')
    
    try:
        client.create_dataset(f'{client.project}.{dataset_id}')
        print(f'Created dataset {client.project}.{dataset_id}')
    except exceptions.Conflict:
        print('Dataset already exists.')

def get_tbl_id(csv_path):
    file = os.path.split(csv_path)[-1]
    tbl_id = file[:-len('-yyyymmdd.csv')].replace('-','_')
    return tbl_id

def load_raw_data(csv_path):
    client = bigquery.Client()
    table_id = get_tbl_id(csv_path)
    raw_tbl_ref = client.dataset(dataset_id)\
                        .table('raw_'+table_id)
    
    job_config = bigquery.LoadJobConfig()
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.skip_leading_rows = 1
    job_config.autodetect = True
    
    scheme = csv_path.rpartition('://')[0]
    # scheme gs = google storage
    if scheme == 'gs':
        job = client.load_table_from_uri(csv_path,
                                         destination=raw_tbl_ref,
                                         job_config=job_config)         
    else:
        open_for_readg = 'r'
        binary_mode = 'b'
        with open(csv_path, open_for_readg+binary_mode) as file_obj:
            job = client.load_table_from_file(
                    file_obj,
                    raw_tbl_ref,
                    job_config=job_config
                    )
    job.result(timeout=60)  # Waits for table load to complete.
    print(f"Loaded {job.output_rows} rows into raw_{table_id}.")

def load_transformed_data(table_id):
    client = bigquery.Client()
    dest_tbl = client.get_table(client.dataset(dataset_id)\
                                .table(table_id))
    col_to_dtype = [(s.name, \
                     s.field_type.replace('INTEGER', 'INT64').replace('FLOAT', 'FLOAT64'))\
                        for s in dest_tbl.schema]
    
    cast_stmts = ', \n\t'.join(('cast('+nm+ ' as ' + dt + ') as ' + nm for nm, dt in col_to_dtype))
    transform_stmt = 'select ' + cast_stmts + '\n from ' + dataset_id + '.' + 'raw_' + table_id
    
    columns = list(zip(*col_to_dtype))[0]
    insert_stmt = 'insert into traffic_cam.'+table_id+' '+wrap(', '.join(columns)) + '\n' +\
                                                               transform_stmt
    
    query = client.query(insert_stmt)
    query.result()

def get_arrival_date(csv_path):
    date_start = -len('yyyymmdd.csv')
    yr_end = date_start+4
    m_end = yr_end+2
    d_end = m_end+2
    arrival_date = tuple(map(int,(csv_path[date_start:yr_end],
                                  csv_path[yr_end:m_end],
                                  csv_path[m_end:d_end])))
    return arrival_date

def wrap(text, wrapper='()'):
    return wrapper[0] + text + wrapper[-1]

def record_ingestion(csv_path):
    scheme = csv_path.rpartition('://')[0]
    if scheme == 'gs':
        origin = 'bucket'
    else:
        origin = 'local'
    
    values = (wrap(get_tbl_id(csv_path), "'"),
              'DATE'+str(get_arrival_date(csv_path)),
              wrap(origin,"'"))
    
    insert_stmt = 'insert into traffic_cam.files_ingested(name, arrival_date, origin) \n' + \
                    "\t values "+ wrap(', '.join(values))
    
    query = bigquery.Client().query(insert_stmt)
    query.result()
    print('recorded ingestion of', values)

def ingest_data(path):
    load_raw_data(path)
    table_id = get_tbl_id(path)
    load_transformed_data(table_id)
    record_ingestion(path)

def unzip_file(path):
    try:
        with zipfile.ZipFile(path,"r") as zip_ref:
            zip_ref.extractall()
    except:
        pass

def upload_blob(bucket_name, source_file_path):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    
    destination_blob_name = os.path.split(source_file_path)[-1]
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_path)

    print('File {} uploaded to {}.'.format(
        source_file_path,
        bucket_name))

def make_bucket(bucket_prefix):
    """Creates a new bucket w a unique name and returns bucket"""
    storage_client = storage.Client()
    
    # bucket name must be unique to all the bucket names in existance
    from uuid import uuid4
    bucket_name = bucket_prefix + str(uuid4())

    bucket = storage_client.create_bucket(bucket_name)
    print('Bucket {} created'.format(bucket.name))
    return bucket