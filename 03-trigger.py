import mypipeline
from google.cloud import storage
from google.cloud import bigquery
from datetime import datetime
from datetime import timezone
from datetime import date
from time import sleep
from functools import wraps

my_bucket = tuple(filter(lambda bucket: bucket.id[:10]=='my-bucket-',
                         storage.Client().list_buckets()))[0]

last_ingested_df = bigquery.Client()\
                            .list_rows('traffic_cam.files_ingested')\
                            .to_dataframe().groupby('name').max().reset_index()

def is_blob_latest(blob):
	""" 
	We do not want to ingest files in Google Storage, blobs, that 
	we already ingested and we only want to ingest the latest blobs.
	This function checks if we already ingested a blob. 
	The blob's name includes its arrival date and name, we check
	in our bigquery table files_ingested if we already ingested this blob.  

	"""
	b_arrival_date = date(*mypipeline.get_arrival_date(blob.name))
	b_name = mypipeline.get_tbl_id(blob.name)
	last_arrival_date = last_ingested_df.arrival_date[last_ingested_df.name==b_name].values[0]
	return b_arrival_date > last_arrival_date

# sorting by update time, so that we will ingest files by how long it's been idle/inactive
newest_arrivals = sorted(filter(is_blob_latest, my_bucket.list_blobs()), key=lambda b: b.updated)

def wait_for_idle(ingestion_fn):
    # @wraps is a decorator factory
    # without @wraps, decorated fn would take on name of decorator
    # and docstring of decorated fn would be lost
    @wraps(ingestion_fn)
    def inner(path):
        scheme = path.rpartition('://')[0]
        if scheme == 'gs':
            bucket_name, blob_name = path.split('/')[-2:]
            blob = list(filter(lambda blob: blob.name==blob_name, 
                               storage.Client().get_bucket(bucket_name).list_blobs()))[0]
            get_mins_idle = lambda blob: (datetime.now(timezone.utc)-blob.updated).total_seconds()/60
            wait_mins = 5
            mins_idle = get_mins_idle(blob)
            while mins_idle < wait_mins:
                print(path, 'last touched', mins_idle, 'mins ago; waiting before ingesting file...')
                sleep(15)
                mins_idle = get_mins_idle(blob)
        return ingestion_fn(path)
    return inner

wait_ingest_data = wait_for_idle(mypipeline.ingest_data)

def main():
	print('ingesting', newest_arrivals)
	for i, f in enumerate(newest_arrivals):
	    print('ingesting', f.name)
	    uri = 'gs://' + f.bucket.name + '/' + f.name
	    print(uri)
	    wait_ingest_data(uri)

if __name__ == '__main__':
	main()