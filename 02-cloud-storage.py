import mypipeline
import os

def make_my_bucket(folder='csv-files', bucket_prefix='my-bucket-'):
    print('Creating bucket in Google Cloud Storage...')
    my_bucket = mypipeline.make_bucket(bucket_prefix)
    
    for i, f in enumerate(os.listdir(folder)):
        print('uploading', i, '/', len(os.listdir(folder)), '...')
        mypipeline.upload_blob(my_bucket.name, os.path.join(folder, f))
        
    return my_bucket

if __name__ == '__main__':
	make_my_bucket()