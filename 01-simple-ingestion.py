import mypipeline
import sys

mypipeline.unzip_file('csv-files.zip')
csv_path = sys.argv[1]

if __name__ == '__main__':
	mypipeline.ingest_data(csv_path)