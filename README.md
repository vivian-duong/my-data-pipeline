# My Data Pipeline
I design an ingestion pipeline using the Google Cloud Platform for the Kaggle dataset CSV files and store them in BigQuery tables.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them.
* Python 3.6+  
* A Google Cloud Platform  
** There is a generous free trial  
** cloud.google.com/free

### Installing
Clone this repo. 
`git clone https://gitlab.com/vivian-duong/my-data-pipeline.git`

### Running/Testing The Code
Open your [Google Cloud Shell](https://cloud.google.com/shell/). If you have a Google Cloud account, you have access to this shell. It provides you command-line access to a (tiny) compute engine pre-installed with Google Cloud SDK and Docker so that you do not need it installed in your system.  

#### If I'm using Cloud Shell, then isn't it unnecessary to use Docker?
Yes. The purpose of a docker container is to package up code and all its dependencies so that an application can run reliably from one computing environment to another. Cloud Shell is a tiny compute engine and is meant to conveniently provide command-line access to Google Cloud, not so much to store and run applications outside of Google Cloud; thus, everybody's Cloud Shell should have a computing environment that is mostly configured the same.  

However, using Docker makes it so that we do not have to use the Cloud Shell to test this repo. We can reliably test it in any computing environment with Docker installed. Without Docker and Cloud Shell, we would have to install Python 3.5+, Google Cloud SDK, and Cloud client libraries for Google BigQuery API and Google Storage API. 

#### Run `04-docker.sh` script
`sh 04-docker.sh [makeup-a-docker-image-name]`  

This shell script accepts an image name, you can name the image whatever you want, as a parameter. It checks if that image has already been built. If not, it will build the Docker container (via the `Dockerfile`). After that, it then invokes my pipeline. Part of invoking my pipeline involves you logging into your Google Cloud account and directions will be provided. *It is setup not to have you make and use a service account rather than a specific user. I understand that I should fix that because service accounts have fewer privileges and privileges that can be rolled back unlike user accounts.* This is especially important because this application is controlling your account. 

##### My Pipeline
######  `python 00-schema.py`
- Creates a database schema in BigQuery to host the CSV files from the Kaggle dataset
- The four tables have appropriately typed columns for the data ie numbers stored as numbers, dates stored as dates, etc.

###### `sh 01-simple-ingestion.sh`
- Bash script runs Python script `01-simple-ingestion py` which accepts a single CSV path as a parameter and inserts the CSV into a table in Big Query
- Bash script calls the Python script to import all the csv files that arrived during or before 2017.
- Simulating a case where by 2017, our csv files that arrived by 2017 will have been uploaded. The rest of the scripts will ingest files arriving after 2017 from Google Cloud Storage, not a local drive.
- `YYYYMMDD` appended to file names is the arrival date

###### `python 02-cloud-storage.py`
- Makes my bucket in Google Cloud Storage and uploads all csv files to the bucket
- Following code will ingest csv files in the bucket

###### `python 03-trigger.py`
- Python script monitors my bucket for newly updated files
-- waits until five minutes of inactivity for files in the directory before it begins to ingest the files
- Only ingests the latest files. 
-- Checks BigQuery table `files_ingested` for whether a file has been uploaded already. When inguesting files in `01-simple-ingestion.sh`, we did record the file being ingested and its arrival date.

## My TO-DOs
### Setup service account in Docker script
### Make it easier for someone new to Google Cloud to know how to verify the code works
