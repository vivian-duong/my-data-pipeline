#!/bin/bash
img_name=$1

# if img_name does not exist, build img_name
docker image inspect $img_name > /dev/null 2>&1 || docker build --tag=$img_name .
docker run -d --name my_container $img_name
# docker exec -it my_container /bin/bash

docker exec -it my_container gcloud init
docker exec -it my_container gcloud auth application-default login

echo 'setting db schema...'
docker exec -it my_container python 00-schema.py

echo 'ingesting files from local drive that arrived during or before 2017...'
docker exec -it my_container bash 01-simple-ingestion.sh

echo 'creating my bucket with all the csv files...'
docker exec -it my_container python 02-cloud-storage.py

echo 'ingesting the latests, so to speak, files from Google Cloud Storage, ie my bucket, that is not being touched...'
docker exec -it my_container python 03-trigger.py