FROM python:3
# TODO: consider usg Google Cloud SDK img so building new images is faster

WORKDIR /usr/src/app

COPY . /usr/src/app

# downloading gcloud package
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz

# Installing the package
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh; \
  pip install google-cloud-storage;\
  pip install google-cloud-bigquery;\
  pip install pandas

# Adding the package path to local
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

CMD ["bash", "docker-bootstrap.sh", "-d"]